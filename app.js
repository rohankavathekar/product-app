const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const path = require('path');
const db = require('./db');
const collection = "products";

//index
app.get('/',(req,res)=>{
  res.sendFile(path.join(__dirname,'index.html'));
});

//read
app.get('/getProducts',(req,res)=>{
  db.getDB().collection(collection).find({}).toArray((err,documents)=>{
    if(err)
      console.log(err);
    else{
      res.json(documents);
    }
  });
});

//update
app.put('/:id',(req,res)=>{
  const productID = req.params.id;
  const userInput = req.body;
  db.getDB().collection(collection).findOneAndUpdate({_id : db.getPrimaryKey(productID)},{$set : {product : userInput.product}},{returnOriginal : false},(err,result)=>{
    if(err)
      console.log(err);
    else{
      res.json(result);
    }
  });
});

//create
app.post('/',(req,res,next)=>{
  const userInput = req.body;
  db.getDB().collection(collection).insertOne(userInput,(err,result)=>{
  if(err){
    console.log(err);
  }
  else
    res.json({result : result, document : result.ops[0]});
  });
});

//delete
app.delete('/:id',(req,res)=>{
  const todoID = req.params.id;
  db.getDB().collection(collection).findOneAndDelete({_id : db.getPrimaryKey(productID)},(err,result)=>{
        if(err)
            console.log(err);
        else
            res.json(result);
    });
});

//connect database
db.connect((err)=>{
  if(err){
    console.log('unable to connect to database');
    process.exit(1);
  }
  else{
    app.listen(3000,()=>{
    console.log('connected to database, app Rockin on port 3000');
    });
  }
});